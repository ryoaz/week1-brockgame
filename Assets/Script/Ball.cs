﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    private float speed = 3.5f;
    private float addSpeed = 0.5f;

    private int blockCount = 0;

    public GameObject retry;

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody>().AddForce(speed, -speed, 0, ForceMode.VelocityChange);

        retry.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if (blockCount == 15)
        {
            blockCount = 0;

            retry.SetActive(true);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Block"))
        {
            Destroy(collision.gameObject);

            blockCount++;
            Debug.Log(blockCount);
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

            blockCount = 0;

            retry.SetActive(true);
        }

        if (collision.gameObject.CompareTag("RightRacket"))
        {
            GetComponent<Rigidbody>().AddForce(-addSpeed, addSpeed, 0, ForceMode.VelocityChange);
        }

        if (collision.gameObject.CompareTag("LeftRacket"))
        {
            GetComponent<Rigidbody>().AddForce(addSpeed, addSpeed, 0, ForceMode.VelocityChange);
        }
    }

}
