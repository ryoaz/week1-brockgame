﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacketContlloer : MonoBehaviour {

    private float speed = 0.1f;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.RightArrow) && gameObject.transform.position.x <= 1.64){
            gameObject.transform.Translate(speed, 0, 0);
        }
        else if (Input.GetKey(KeyCode.LeftArrow) && gameObject.transform.position.x >= -1.64)
        {
            gameObject.transform.Translate(-speed, 0, 0);
        }
    }
}
